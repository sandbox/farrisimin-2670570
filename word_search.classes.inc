<?php
/**
 * Extension of QuizQuestion.
 *
 *
 */
class WordSearchQuestion extends QuizQuestion {

  // Constants for answer matching options
  const ANSWER_MATCH = 0;
  const ANSWER_INSENSITIVE_MATCH = 1;
  const ANSWER_REGEX = 2;
  const ANSWER_MANUAL = 3;
  /**
   * Implementation of saveNodeProperties
   *
   * @see QuizQuestion#saveNodeProperties($is_new)
   */
  public function saveNodeProperties($is_new = FALSE) {
    $config = $this->node->word_search;
    $id = db_merge('quiz_word_search_node_properties')
          ->key(array(
              'nid' => $this->node->nid,
              'vid' => $this->node->vid
             )
            )
          ->fields(array(
            //'word_search_answers' => $config['word_search_answers'],
            'word_search_question' => $config['word_search_question'],
            'word_search_rows' => $config['word_search_rows'],
            'word_search_cols' => $config['word_search_cols'],
            'word_search_nowords' => $config['word_search_nowords'],
            'word_search_lang' => $config['word_search_lang'],
            'word_search_gridStyle' => $config['word_search_gridStyle'],
            'word_search_fontsize' => $config['word_search_fontsize'],
            'word_search_fontcolor' => $config['word_search_fontcolor'],
            'word_search_bgcolor' => $config['word_search_bgcolor'],
            'word_search_hlcolor' => $config['word_search_hlcolor'],
            'word_search_lowercase' => $config['word_search_lowercase'],
            'word_search_sortalpha' => $config['word_search_sortalpha'],
            'word_search_forward_backward' => $config['word_search_forward_backward'],
            'word_search_usewordlist' => $config['word_search_usewordlist'],
            'word_search_diagnals' => $config['word_search_diagnals'],
            'word_search_upanddown' => $config['word_search_upanddown'],
            'word_search_placementword' => $config['word_search_placementword'],
            //'word_search_rows' => $this->node->correct_answer,
            //'correct_answer_evaluation' => $this->node->correct_answer_evaluation,
            )
         )
         ->execute();
  }
  /**
   * Implementation of validateNode
   *
   * @see QuizQuestion#validateNode($form)
   */
  public function validateNode(array &$form) {
  //public  function myModule_signup_validate($form, &$form_state) {
     //$formValues = $form_state['values'];
      //if (empty($formValues['word_search_rows'])) {
    //if ($this->node->correct_answer_evaluation != self::ANSWER_MANUAL && empty($this->node->correct_answer)) {
      //form_set_error('correct_answer', t('An answer must be specified for any evaluation type other than manual scoring.'));
    //}
   // form_set_error('word_search_rows',t('hello'));

    //  }
  }

  /**
   * Implementation of delete
   *
   * @see QuizQuestion#delete($only_this_version)
   */
  public function delete($only_this_version = FALSE) {
    parent::delete($only_this_version);
    $delete_ans = db_delete('quiz_word_search_user_answers');
    $delete_ans->condition('question_nid', $this->node->nid);
    $delete_node = db_delete('quiz_word_search_node_properties');
    $delete_node->condition('nid', $this->node->nid);
    if ($only_this_version) {
      $delete_ans->condition('question_vid', $this->node->vid);
      $delete_node->condition('vid', $this->node->vid);
    }
    $delete_ans->execute();
    $delete_node->execute();
  }

  /**
   * Implementation of getNodeProperties
   *
   * @see QuizQuestion#getNodeProperties()
   */
  public function getNodeProperties() {
    if (isset($this->nodeProperties)) {
      return $this->nodeProperties;
    }
    $props = parent::getNodeProperties();
    $res_a = db_query('SELECT  word_search_rows, word_search_cols, word_search_nowords, word_search_lang, word_search_gridStyle,
      word_search_fontsize, word_search_fontcolor, word_search_bgcolor, word_search_hlcolor, word_search_lowercase, word_search_sortalpha, word_search_usewordlist, word_search_diagnals, word_search_upanddown, word_search_placementword,
      word_search_forward_backward, word_search_question FROM {quiz_word_search_node_properties}
      WHERE nid = :nid AND vid = :vid', array(':nid' => $this->node->nid, ':vid' => $this->node->vid))->fetchAssoc();
    $props['word_search'] = $res_a;
    $this->nodeProperties =  $props;
    return $this->nodeProperties;
  }

  /**
   * Implementation of getNodeView
   *
   * @see QuizQuestion#getNodeView()
   */
  public function getNodeView() {
    $content = parent::getNodeView();
      if ($this->viewCanRevealCorrect()) {
        $content['answers'] = array(
        '#markup' => '<div class="quiz-solution"></div>',
        '#weight' => 2,
      );
    }
    else {
      $content['answers'] = array(
        '#markup' => '<div class="quiz-answer-hidden">Answer hidden</div>',
        '#weight' => 2,
      );
    }
    return $content;
  }

  /**
   * Implementation of getAnsweringForm
   *
   * @see QuizQuestion#getAnsweringForm($form_state, $rid)
   */
  public function getAnsweringForm(array $form_state = NULL, $rid) {
    $form = parent::getAnsweringForm($form_state, $rid);
    $response = '';
    if (isset($rid)) {
      // The question has already been answered. We load the answers
      $response = new WordSearchResponse($rid, $this->node);
      $response = $response->getResponse();
    }
    $config = $this->node->word_search;
    drupal_add_js(drupal_get_path('module','word_search') . '/jsWordsearch_files/options.js');
    drupal_add_js(drupal_get_path('module','word_search') . '/jsWordsearch_files/languages.js');
    drupal_add_js(drupal_get_path('module','word_search') . '/jsWordsearch_files/gridstyles.js');
    drupal_add_js(drupal_get_path('module','word_search') . '/jsWordsearch_files/wordlists.js');
    drupal_add_js(drupal_get_path('module','word_search') . '/jsWordsearch_files/mainlanguages.js');
    drupal_add_js(drupal_get_path('module','word_search') . '/jsWordsearch_files/wordsearch.js');
    drupal_add_js(drupal_get_path('module','word_search') . '/quiz_wordsearch.js');
  $form['words']  = array(
    '#type' => 'textarea',
    '#description' => t('Words for Puzzle'),
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#attributes' => array(
      'id' => 'inputWords',
      'class' => array('word-search-hidden-field')
      ),
    '#default_value' => $config['word_search_question'],
    '#access' => TRUE
  );
  $form['optionsrows'] = array(
    '#type' => 'textfield',
    '#title' => t('No of rows'),
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#attributes' => array(
      'id' => 'numRows',
      'class' => array('word-search-hidden-field')
     ),
    '#access' => TRUE,
    '#default_value' => $config['word_search_rows']
  );
  $form['optionscols'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('No of Columns'),
    '#attributes' => array(
      'id' => 'numCols',
      'class' => array('word-search-hidden-field')
     ),
    '#access' => TRUE,
    '#default_value' => $config['word_search_cols']
  );
  $form['numWords'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('No of Words'),
    '#attributes' => array(
    'id' => 'numWords',
    'class' => array('word-search-hidden-field')
    ),
    '#access' => TRUE,
  );
  $form['language'] = array(
    '#type' => 'select',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#options' => array(
      'English' => t('English'),
    ),
    '#default_value' => 'English',
    '#access' => TRUE,
    '#attributes' => array(
      'id' => 'gridLanguage',
      'class' => array('word-search-hidden-field')
    )
  );
  $form['gridStyle'] = array(
    '#type' => 'select',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#options' => array(
      'square' => t('Square'),
      '15x22 Car' => t('15x22 Car'),
      '20x20 Diamond' => t('20x20 Diamond'),
      '20x20 Spiral' => t('20x20 Spiral'),
      '20x20 Star' => t('20x20 Star'),
      '20x20 X' => t('20x20 X'),
      '19x29 Train' => t('19x29 Train'),
      '24x24 HardOCP' => t('24x24 HardOCP'),
      '25x25 Clover' => t('25x25 Clover'),
      '25x25 Stripes' => t('25x25 Stripes'),
      '27x27 Circle' => t('27x27 Circle'),
      '28x30 V' => t('28x30 V'),
      '30x30 Triangle' => t('30x30 Triangle'),
      '40x40 Checker' => t('40x40 Checker'),
      '58x59 Spiral' => t('58x59 Spiral'),
      '27x27 custom' => t('Custom'),
    ),
    '#default_value' => $config['word_search_gridStyle'],
    '#access' => TRUE,
    '#attributes' => array(
      'id' => 'gridStyle',
      'class' => array('word-search-hidden-field')
    )
  );
  $form['fontSize'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('fontSize'),
    '#attributes' => array(
      'id' => 'fontSize',
      'class' => array('word-search-hidden-field')
    ),
    '#default_value' => $config['word_search_fontsize'],
    '#access' => TRUE
  );
  $form['fontColor'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('fontColor'),
    '#attributes' => array(
      'id' => 'fontColor',
      'class' => array('word-search-hidden-field')
  ),
    '#access' => TRUE,
    '#default_value' => $config['word_search_fontcolor'],
  );
  $form['bgColor'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('bgColor'),
    '#attributes' => array(
      'id' => 'bgColor',
      'class' => array('word-search-hidden-field')
    ),
    '#access' => TRUE,
    '#default_value' => $config['word_search_bgcolor']
  );
  $form['hlColor'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('hlColor'),
    '#attributes' => array(
      'id' => 'hlColor',
      'class' => array('word-search-hidden-field')
    ),
    '#default_value' => $config['word_search_hlcolor'],
    '#access' => TRUE
  );
  $form['lowerCase'] = array(
    '#type' => 'checkbox',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('lowerCase'),
    '#default_value' => 0,
    '#attributes' => array(
      'id' => 'lowerCase',
      'class' => array('word-search-hidden-field')
    ),
    '#access' => TRUE,
    '#default_value' => $config['word_search_lowercase'],
  );
  $form['sortAlpha'] = array(
    '#type' => 'checkbox',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('sortAlpha'),
    '#default_value' => 0,
    '#attributes' => array(
      'id' => 'sortAlpha',
      'class' => array('word-search-hidden-field')
    ),
    '#access' => TRUE,
    '#default_value' => $config['word_search_sortalpha'],
  );
  $form['useLetters'] = array(
    '#type' => 'checkbox',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('useLetters'),
    '#default_value' => 0,
    '#attributes' => array(
      'id' => 'useLetters',
      'class' => array('word-search-hidden-field')
    ),
    '#access' => TRUE,
    '#default_value' => $config['word_search_usewordlist'],
  );
  $form['Fowards and BackwardsFowards and Backwards'] = array(
    '#type' => 'select',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#options' => array(
      'regular' => t('Fowards and Backwards'),
      'bonly' => t('Backwards only'),
      'fonly' => t('Forwards only')
    ),
    '#attributes' => array(
      'id' => 'BorF',
      'class' => array('word-search-hidden-field')
    ),
    '#default_value' => $config['word_search_forward_backward'],
  );
  $form['Diagonal words'] = array(
    '#type' => 'select',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#options' => array(
      'diag' => t('Diagonal words'),
      'diagonly' => t('Diagonal words only'),
      'nodiag' => t('No diagonal words')
    ),
    '#attributes' => array(
      'id' => 'diag',
      'class' => array('word-search-hidden-field')
    ),
    '#default_value' => $config['word_search_diagnals'],
  );
  $form['Up and down words'] = array(
    '#type' => 'select',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#options' => array(
      'uand' => t('Up and down words'),
      'uandonly' => t('Up and down words only'),
      'nouand' => t('No up and down words')
    ),
    '#attributes' => array(
      'id' => 'uand',
      'class' => array('word-search-hidden-field')
    ),
    '#default_value' => $config['word_search_upanddown'],
  );
  $form['Placement of word list'] = array(
    '#type' => 'select',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('Placement of word list'),
    '#options' => array(
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
    '#attributes' => array(
      'id' => 'wordListPlacement',
      'class' => array('word-search-hidden-field')
    ),
    '#default_value' => $config['word_search_placementword'],
  );
  $form['tries'] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#attributes' => array(
      'id' => 'edit-tries',
      'class' => array('word-search-hidden-field')
    ),
    '#access' => TRUE,
    '#default_value' => $response
  );
  $form['puzzle'] = array(
    '#markup' => '<div id="puzzleText">Lorem ipsum</div>'
  );
  if (isset($rid)) {
    $response = new WordSearchResponse($rid, $this->node);
     }
   //print_r($config['word_search_bgcolor']);
  return $form;
  }
  /**
   * Implementation of getCreationForm
   *
   * @see QuizQuestion#getCreationForm($form_state)
   */
  public function getCreationForm(array &$form_state = NULL) {
   //global $this;
    $config = isset($this->node->word_search) ? $this->node->word_search : array();
  drupal_add_js(drupal_get_path('module', 'word_search') . '/jquery-colorpicker/jquery.colorPicker.js');
  drupal_add_css(drupal_get_path('module', 'word_search') . '/jquery-colorpicker/colorPicker.css');
  drupal_add_js(drupal_get_path('module', 'word_search') . '/word_search_creation_form.js');
  $form['word_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Word Search'),
    '#tree' => TRUE,
    '#theme' => 'word_search_question_form',
    '#description' => t('Write your quiz puzzles and number of rows and columns.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
    //the comma separated value for creating puzzle
  $form['word_search']['word_search_question'] = array(
    '#title' => t('Quiz Puzzle'),
    '#type' => 'textfield',
    '#description' => t('Enter the comma seperated words for creating puzzle. example : ball,cat'),
    '#default_value' => isset($config['word_search_question']) ? $config['word_search_question'] : array(),
    '#required' => TRUE,
    '#maxlength' => 225,
  );
  $form['word_search']['word_search_rows'] = array(
    '#title' => t('Rows'),
    '#type' => 'textfield',
    '#description' => t('No of Rows'),
    '#required' => TRUE,
    '#default_value' => isset($config['word_search_rows']) ? $config['word_search_rows'] : 5
  );
  $form['word_search']['word_search_cols'] = array(
    '#title' => t('Columns'),
    '#type' => 'textfield',
    '#description' => t('No of Columns'),
    '#required' => TRUE,
    '#default_value' => isset($config['word_search_cols']) ? $config['word_search_cols'] : 5
  );
  $form['word_search']['word_search_nowords'] = array(
    '#type' => 'textfield',
    '#title' => t('No of Words'),
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#default_value' => isset($config['word_search_nowords']) ? $config['word_search_nowords'] : 25,
    '#attributes' => array(
      'id' => 'numWords',
      'class' => array('word-search-hidden-field')
    ),
    '#access' => TRUE
  );
  $form['word_search']['word_search_lang'] = array(
    '#type' => 'select',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('Language'),
    '#options' => array(
      'English' => t('English'),
    ),
    '#default_value' => 'English',
    '#access' => TRUE,
    '#attributes' => array(
      'id' => 'gridLanguage',
      'class' => array('word-search-hidden-field')
    ),
  );
  $form['word_search']['word_search_gridStyle'] = array(
    '#type' => 'select',
    '#title' => t('Puzzle Shape'),
    '#options' => array(
      'square' => t('Square'),
      '15x22 Car' => t('15x22 Car'),
      '20x20 Diamond' => t('20x20 Diamond'),
      '20x20 Spiral' => t('20x20 Spiral'),
      '20x20 Star' => t('20x20 Star'),
      '20x20 X' => t('20x20 X'),
      '19x29 Train' => t('19x29 Train'),
      '24x24 HardOCP' => t('24x24 HardOCP'),
      '25x25 Clover' => t('25x25 Clover'),
      '25x25 Stripes' => t('25x25 Stripes'),
      '27x27 Circle' => t('27x27 Circle'),
      '28x30 V' => t('28x30 V'),
      '30x30 Triangle' => t('30x30 Triangle'),
      '40x40 Checker' => t('40x40 Checker'),
      '58x59 Spiral' => t('58x59 Spiral'),
    ),
    '#default_value' => isset($config['word_search_gridStyle']) ? $config['word_search_gridStyle'] : 'square',
    '#access' => TRUE,
    '#attributes' => array(
    'id' => 'gridStyle',
    )
  );
  $form['word_search']['word_search_fontsize'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div class='word-search-hidden-field'>",
    '#suffix' => "</div>",
    '#title' => t('Font Size'),
    '#attributes' => array(
      'id' => 'fontSize',
      'class' => array('word-search-hidden-field')
    ),
    '#default_value' => isset($config['word_search_fontsize']) ? $config['word_search_fontsize'] : 4,
    '#access' => TRUE
  );
  $form['word_search']['word_search_fontcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Color'),
    '#attributes' => array(
      'id' => 'fontColor',
      'class' => array('word-search-color-picker')
    ),
    '#access' => TRUE,
    '#default_value' => isset($config['word_search_fontcolor']) ? $config['word_search_fontcolor'] : '#000000',
  );
  $form['word_search']['word_search_bgcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Color'),
    '#attributes' => array(
      'id' => 'bgColor',
      'class' => array('word-search-color-picker')
    ),
    '#description' =>  '<div id="bgcolor_colorpicker"></div>',
    '#access' => TRUE,
    '#default_value' => isset($config['word_search_bgcolor']) ? $config['word_search_bgcolor'] : '#EFEFEF',
  );
  $form['word_search']['word_search_hlcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Highlight Color'),
    '#attributes' => array(
      'id' => 'hlColor',
      'class' => array('word-search-color-picker')
    ),
    '#description' =>  '<div id="hlcolor_colorpicker"></div>',
    '#access' => TRUE,
    '#default_value' => isset($config['word_search_hlcolor']) ? $config['word_search_hlcolor'] : '#808080',
  );
  $form['word_search']['word_search_lowercase'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lowercase letters'),
    '#default_value' => isset($config['word_search_lowercase']) ? $config['word_search_lowercase'] : '0',
    '#attributes' => array(
      'id' => 'lowerCase',
    ),
    '#access' => TRUE,
  );
  $form['word_search']['word_search_sortalpha'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sort alphabetically'),
    '#default_value' => isset($config['word_search_sortalpha']) ? $config['word_search_sortalpha'] : '0',
    '#attributes' => array(
      'id' => 'sortAlpha',
    ),
    '#access' => TRUE
  );
  $form['word_search']['word_search_usewordlist'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use word list letters'),
    '#default_value' => isset($config['word_search_usewordlist']) ? $config['word_search_usewordlist'] : '0',
    '#attributes' => array(
      'id' => 'useLetters',
    ),
    '#access' => TRUE
  );
  $form['word_search']['word_search_forward_backward'] = array(
    '#type' => 'select',
    '#default_value' => isset($config['word_search_forward_backward']) ? $config['word_search_forward_backward'] : 'regular',
    '#options' => array(
      'regular' => t('Fowards and Backwards'),
      'bonly' => t('Backwards only'),
      'fonly' => t('Forwards only')
    ),
    '#attributes' => array(
       'id' => 'BorF',
    ),
  );
  $form['word_search']['word_search_diagnals'] = array(
    '#type' => 'select',
    '#default_value' => isset($config['word_search_diagnals']) ? $config['word_search_diagnals'] : 'diag',
    '#options' => array(
      'diag' => t('Diagonal words'),
      'diagonly' => t('Diagonal words only'),
      'nodiag' => t('No diagonal words')
    ),
    '#attributes' => array(
      'id' => 'diag',
    ),
  );
  $form['word_search']['word_search_upanddown'] = array(
    '#type' => 'select',
    '#default_value' => isset($config['word_search_upanddown']) ? $config['word_search_upanddown'] : 'uand',
    '#options' => array(
      'uand' => t('Up and down words'),
      'uandonly' => t('Up and down words only'),
      'nouand' => t('No up and down words')
    ),
    '#attributes' => array(
      'id' => 'uand',
    ),
  );
  $form['word_search']['word_search_placementword'] = array(
    '#type' => 'select',
    '#default_value' => isset($config['word_search_placementword']) ? $config['word_search_placementword'] : 'right',
    '#title' => t('Placement of word list'),
    '#options' => array(
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
    '#attributes' => array(
      'id' => 'wordListPlacement',
    ),
  );
    return $form;
  }
  /**
   * Implementation of getMaximumScore
   *
   * @see QuizQuestion#getMaximumScore()
   */
  public function getMaximumScore() {
    $config = $this->node->word_search;
    $question = drupal_strtolower($config['word_search_question']);
    $questions = explode(",", $question);
    return count($questions);
  }

  /**
   * Evaluate the correctness of an answer based on the correct answer and evaluation method.
   */
  public function evaluateAnswer($user_answer) {
    $score = 0;
    $config = $this->node->word_search;
    $question = drupal_strtolower($config['word_search_question']);
    $questions = explode(",", $question);
    $totcount = count($questions);
    $answers = explode(",", drupal_strtolower($user_answer));
    //$score = 0;
    for ($i = 0; $i < count($questions); $i++) {
      if (in_array($questions[$i], $answers)) {
        $score += 1 ;
        //$score += $this->score = $r->score;
      }
    }
    return $score;
  }
}

/**
 * Extension of QuizQuestionResponse
 */
class WordSearchResponse extends QuizQuestionResponse {
  /**
   * Get all quiz scores that haven't been evaluated yet.
   *
   * @param $count
   *  Number of items to return (default: 50).
   * @param $offset
   *  Where in the results we should start (default: 0).
   *
   * @return
   *  Array of objects describing unanswered questions. Each object will have result_id, question_nid, and question_vid.
   */
  public static function fetchAllUnscoredAnswers($count = 50, $offset = 0) {
    global $user;
    $query = db_select('quiz_word_search_user_answers', 'a');
    $query->fields('a', array('result_id', 'question_nid', 'question_vid', 'answer_feedback'));
    $query->fields('r', array('title'));
    $query->fields('qnr', array('time_end', 'time_start', 'uid'));
    $query->join('node_revision', 'r', 'a.question_vid = r.vid');
    $query->join('quiz_node_results', 'qnr', 'a.result_id = qnr.result_id');
    $query->join('node', 'n', 'qnr.nid = n.nid');
    $query->condition('a.is_evaluated', 0);

    if (user_access('score own quiz') && user_access('score taken quiz answer')) {
      $query->condition(db_or()->condition('n.uid', $user->uid)->condition('qnr.uid', $user->uid));
    }
    else if (user_access('score own quiz')) {
      $query->condition('n.uid', $user->uid);
    }
    else if (user_access('score taken quiz answer')) {
      $query->condition('qnr.uid', $user->uid);
    }
    $results = $query->execute();
    $unscored = array();
    foreach ($results as $row) {
      $unscored[] = $row;
    }
    return $unscored;
  }

  /**
   * Given a question, return a list of all of the unscored answers.
   *
   * @param $nid
   *  Node ID for the question to check.
   * @param $vid
   *  Version ID for the question to check.
   * @param $count
   *  Number of items to return (default: 50).
   * @param $offset
   *  Where in the results we should start (default: 0).
   *
   * @return
   *  Indexed array of result IDs that need to be scored.
   */
  public static function fetchUnscoredAnswersByQuestion($nid, $vid, $count = 50, $offset = 0) {
    return db_query('SELECT result_id FROM {quiz_word_search_user_answers}
      WHERE is_evaluated = :is_evaluated AND question_nid = :question_nid AND question_vid = :question_vid',
      array(':is_evaluated' => 0, ':question_nid' => $nid, ':question_vid' => $vid)
    )->fetchCol();

  }

  /**
   * ID of the answer.
   */
  protected $answer_id = 0;

  /**
   * Constructor
   */
  public function __construct($result_id, stdClass $question_node, $answer = NULL) {
    parent::__construct($result_id, $question_node, $answer);
    if (!isset($answer)) {
      $r = db_query('SELECT answer_id, answer, is_evaluated, score, question_vid, question_nid, result_id,
        answer_feedback
        FROM {quiz_word_search_user_answers}
        WHERE question_nid = :qnid AND question_vid = :qvid AND result_id = :rid',
        array(':qnid' => $question_node->nid, ':qvid' => $question_node->vid, ':rid' => $result_id)
      )->fetch();

      if (!empty($r)) {
        $this->answer = $r->answer;
        $this->score = $r->score;
        $this->evaluated = $r->is_evaluated;
        $this->answer_id = $r->answer_id;
        $this->answer_feedback = $r->answer_feedback;
      }
    }
    else {
      if (is_array($answer)) {
        $this->answer = $answer['answer'];
      }
      else {
        $this->answer = $answer;
       $this->evaluated = TRUE; //$this->question->correct_answer_evaluation != WordSearchQuestion::ANSWER_MANUAL;
      }
    }
  }

  /**
   * Implementation of isValid
   *
   * @see QuizQuestionResponse#isValid()
   */
  public function isValid() {
    if (trim($this->answer) == '') {
      return t('You must provide an answer');
    }
    return TRUE;
  }

  /**
   * Implementation of save
   *
   * @see QuizQuestionResponse#save()
   */
  public function save() {
    // We need to set is_evaluated depending on whether the type requires evaluation.
    $this->is_evaluated = 1;//(int) ($this->question->correct_answer_evaluation != WordSearchQuestion::ANSWER_MANUAL);
    $this->answer_id = db_merge('quiz_word_search_user_answers')
    ->key(array(
              'question_nid' => $this->question->nid,
              'question_vid' => $this->question->vid,
              'result_id' => $this->rid,
             )
            )
      ->fields(array(
        'answer' => $this->answer,
        'score' => $this->getScore(FALSE),
        'is_evaluated' => $this->is_evaluated,
      ))
      ->execute();
  }

  /**
   * Implementation of delete
   *
   * @see QuizQuestionResponse#delete()
   */
  public function delete() {
    db_delete('quiz_word_search_user_answers')
      ->condition('question_nid', $this->question->nid)
      ->condition('question_vid', $this->question->vid)
      ->condition('result_id', $this->rid)
      ->execute();
  }

  /**
   * Implementation of score
   *
   * @see QuizQuestionResponse#score()
   */
  public function score() {
    // Manual scoring means we go with what is in the DB.
    $wordsearchAnswer = new WordSearchQuestion($this->question);
    $score = $wordsearchAnswer->evaluateAnswer($this->getResponse());
    return $score;
  }

  /**
   * Implementation of getResponse
   *
   * @see QuizQuestionResponse#getResponse()
   */
  public function getResponse() {
    return $this->answer;
  }

  /**
   * Implementation of getReportFormResponse
   *
   * @see QuizQuestionResponse#getReportFormResponse($showpoints, $showfeedback, $allow_scoring)
   */
  public function getReportFormResponse($showpoints = TRUE, $showfeedback = TRUE, $allow_scoring = FALSE) {
    $form = array();
    $config = $this->question->word_search;
    $given_words = $config['word_search_question'];
    $form['#theme'] = 'word_search_response_form';
    if ($this->question && !empty($this->question->answers)) {
      $answer = (object) current($this->question->answers);
    }
    else {
      return $form;
    }

    $form['answer'] = array(
      '#markup' => theme('word_search_user_answer', array('answer' => check_plain($answer->answer), 'correct' => $given_words)),
      //'#markup' => theme('word_search_user_answer', array('answer' => check_plain($answer->answer), 'correct' => check_plain($this->question->correct_answer))),
    );

    if ($answer->is_evaluated == 1) {
      // Show feedback, if any.
      if ($showfeedback && !empty($answer->feedback)) { // @todo: Feedback doesn't seem to be in use anymore...
        $feedback = check_markup($answer->feedback);
      }
    }
    else {
      $feedback = t('This answer has not yet been scored.') .
        '<br/>' .
      t('Until the answer is scored, the total score will not be correct.');
    }

    if (!$allow_scoring && !empty($this->answer_feedback)) {
      $form['answer_feedback'] = array(
        '#title' => t('Feedback'),
        '#type' => 'item',
        '#markup' => '<span class="quiz_answer_feedback">' . $this->answer_feedback . '</span>',
      );
    }
    if (!empty($feedback)) {
      $form['feedback'] = array(
        '#markup' => '<span class="quiz_answer_feedback">' . $feedback . '</span>',
      );
    }
    return $form;
  }

  /**
   * Implementation of getReportFormScore
   *
   * @see QuizQuestionResponse#getReportFormScore($showpoints, $showfeedback, $allow_scoring)
   */
  public function getReportFormScore($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE) {
    $score = ($this->isEvaluated()) ? $this->getScore() : '?';
    if (quiz_access_to_score() && $allow_scoring) {
      return array(
        '#type' => 'textfield',
        '#default_value' => $score,
        '#size' => 3,
        '#maxlength' => 3,
        '#attributes' => array('class' => array('quiz-report-score')),
      );
    }
    else {
      return array(
        '#markup' => $score,
      );
    }
  }

  public function getReportFormAnswerFeedback($showpoints = TRUE, $showfeedback = TRUE, $allow_scoring = FALSE) {
    if (quiz_access_to_score() && $allow_scoring && $this->question->correct_answer_evaluation == WordSearchQuestion::ANSWER_MANUAL) {
      return array(
        '#title' => t('Enter feedback'),
        '#type' => 'textarea',
        '#default_value' => $this->answer_feedback,
        '#attributes' => array('class' => array('quiz-report-score')),
      );
    }
    return FALSE;
  }

  /**
   * Implementation of getReportFormSubmit
   *
   * @see QuizQuestionResponse#getReportFormSubmit($showfeedback, $showpoints, $allow_scoring)
   */
  public function getReportFormSubmit($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE) {
    if (quiz_access_to_score() && $allow_scoring) {
      return $allow_scoring ? 'word_search_report_submit' : FALSE;
    }
    return FALSE;
  }

  /**
   * Implementation of getReportFormValidate
   *
   * @see QuizQuestionResponse#getReportFormValidate($showfeedback, $showpoints, $allow_scoring)
   */
  public function getReportFormValidate($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE) {
    if (quiz_access_to_score() && $allow_scoring) {
      return $allow_scoring ? 'word_search_report_validate' : FALSE;
    }
    return FALSE;
  }
}
