// title that's shown above the options form

var title = 'jsWordsearch';

// defaults

var defRows = 20;

var defCols = 20;

var defFontSize = '4.0';

var defFontColor = 'black';

var defBGColor = '#999999';

var defHighlightColor = '#DDDDDD';

// number of tries to see if the script can fit a word into the puzzle

var retries = 100;

// change to 1 to enable, 0 to disable
// you will need the word list addon

var wordlists = 1;
