(function ($) {
  
Drupal.behaviors.quiz_wordsearch =  {
  attach : function (context, settings) {
    var answers = jQuery('#edit-tries').val();
   // console.log(answers);
    if (answers) {
      createPuzzle(0);
      answers = answers.split(",");
      for(var i=0; i < answers.length; i ++) {
        $('#' + answers[i] + 'r').mousedown();  
      }
    }
    else {    
      createPuzzle(1);
    }
      var d = document;  
      d.onkeypress=keycheck;
      d.onmousedown=omd;
      d.onmouseup=omu;
      d.onselectstart=omd;
  },
};
})(jQuery);

function quiz_wordsearch_answer(answer) {
  var answers = jQuery('#edit-tries').val();
  if (answers) {
    answers = answers + "," + answer;
  }
  else {
    answers = answer;
  }
  jQuery('#edit-tries').val(answers);
}
