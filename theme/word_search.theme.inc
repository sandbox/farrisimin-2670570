<?php

/**
 * @file
 * Theme functions for word_search.
 */

/**
 * Theme the list of unscored word search questions.
 *
 * @param $unscored
 *  An array of objects with information about the unscored questions
 */
function theme_word_search_view_unscored($variables) {
  $unscored = $variables['unscored'];
  $output = '';
  $header = array(
    t('Question'),
    t('Time Finished'),
    t('Action')
  );
  $rows = array();

  foreach ($unscored as $item) {
    if ($item->time_end > 0) {
      $rows[] = array(
        $item->title,
        date('Y-m-d H:i', $item->time_end),
        l(t('score this response'), 'admin/quiz/reports/score-word-search/' . $item->question_vid . '/' . $item->result_id),
      );
    }
  }

  if (!empty($rows)) {
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    $output .= t('There are no unscored essays.');
  }
  return $output;
}
function theme_word_search_question_form($variables) {
  $form = $variables['form'];
  drupal_render_children($form);
  return;
  
  $rows = array();
  $header = array(
    'question' => t('Question'),
    'answer' => t('Correct answer'),
    'feedback' => t('Feedback'),
  );
  foreach (element_children($form) as $key) {
    $rows[] = array(
      'question' => drupal_render($form[$key]['question']),
      'answer' => drupal_render($form[$key]['answer']),
      'feedback' => drupal_render($form[$key]['feedback']),
    );
  }
  // Theme output and display to screen.
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function theme_word_search_user_answer($variables) {
  $answer = $variables['answer'];
  $correct = $variables['correct'];
  $header = array(t('Given words'), t('Rightly Answered Words'));
  $row = array(array($correct, $answer));
  return theme('table', array('header' => $header, 'rows' => $row));
}

/**
 * Theme the word_search response form
 *
 * @param $form
 *  The response form
 */
function theme_word_search_response_form($variables) {
  $form = $variables['form'];
  return drupal_render_children($form);
}
